package com.example.fabian.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fabian.myapplication.appData.Ingredient;
import com.example.fabian.myapplication.appData.IngredientAdapter;
import com.example.fabian.myapplication.popup_windows.PopUpWindowAddIngredient;
import com.example.fabian.myapplication.popup_windows.PopUpWindowAddName;

import java.util.ArrayList;

/**
 * Class AddMeal, which represents the Activity to add a new Meal
 */
public class AddMeal extends AppCompatActivity {

    public static final int REQUEST_CODE_GET_MEAL_NAME = 99;
    public static final int REQUEST_CODE_GET_INGREDIENT = 100;
    private ArrayList<Ingredient> ingredients;
    private String selectedCategory, personsAmount;
    Button btn_add_edit_MealName, btn_add_Ingredient, btn_save_Meal;
    TextView tv_MealName, tv_persons;
    ImageView ivCategory;
    RecyclerView rv_Ingredients;
    IngredientAdapter ingredientAdapter;
    Intent intent;
    Bundle args;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meal);

        rv_Ingredients = findViewById(R.id.myIngredientsNewMealRecyclerView);
        rv_Ingredients.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        btn_save_Meal =  findViewById(R.id.btn_saveMeal);
        tv_MealName = findViewById(R.id.textView_MealName);
        tv_persons = findViewById(R.id.tv__addMeal_perPerson);
        ivCategory = findViewById(R.id.ivCategoryIcon);
        btn_add_edit_MealName = findViewById(R.id.btn_addName);
        btn_add_Ingredient = findViewById(R.id.btn_myMeals_addIngredient);
        ingredients = new ArrayList<>();
        ingredientAdapter = new IngredientAdapter(getApplicationContext(),ingredients, true, false, null);
        rv_Ingredients.setAdapter(ingredientAdapter);

        btn_save_Meal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ingredients.size() == 0 ||
                        tv_MealName.getText().toString().equals("") ||
                        tv_MealName.getText().toString().equals(getString(R.string.placeholder_Meal_Name))) {
                    Toast.makeText(getApplicationContext(), getString(R.string.addMeal_saveMeal_NoIngredients), Toast.LENGTH_SHORT).show();
                } else {
                    args = new Bundle();
                    args.putSerializable("INGREDIENTS",ingredients);
                    intent = new Intent(getApplicationContext(), MyMeals.class);
                    intent.putExtra("newMealName", tv_MealName.getText().toString());
                    intent.putExtra("newMealCategory", selectedCategory);
                    intent.putExtra("newMealPersons", personsAmount);
                    intent.putExtra("newMealIngredients", args);
                    intent.putExtra("intentFromAddMeal", "IntentFlag");
                    startActivity(intent);
                }
            }
        });
        btn_add_edit_MealName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), PopUpWindowAddName.class);
                startActivityForResult(intent, REQUEST_CODE_GET_MEAL_NAME);
            }
        });
        btn_add_Ingredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), PopUpWindowAddIngredient.class);
                startActivityForResult(intent, REQUEST_CODE_GET_INGREDIENT);
            }
        });
    }

    /**
     * method to react on Result intents
     *
     * @param requestCode requestcode of incoming intent
     * @param resultCode code to show if received intent is ok
     * @param data data of intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode){
            case REQUEST_CODE_GET_MEAL_NAME:
                if(resultCode == Activity.RESULT_OK){
                    tv_MealName.setText(data.getStringExtra("mealName"));
                    selectedCategory = data.getStringExtra("mealCategory");
                    String personAttach = data.getStringExtra("mealPersons") +" "+ getString(R.string.attachment_persons_short);
                    personsAmount = data.getStringExtra("mealPersons");
                    tv_persons.setText(personAttach);
                    if(selectedCategory != null){
                        if(selectedCategory.equals(getString(R.string.stew))){
                            ivCategory.setImageResource(R.mipmap.eintopf);
                        }
                        if(selectedCategory.equals(getString(R.string.fish))){
                            ivCategory.setImageResource(R.mipmap.fish);
                        }
                        if(selectedCategory.equals(getString(R.string.meat))){
                            ivCategory.setImageResource(R.mipmap.cow);
                        }
                        if(selectedCategory.equals(getString(R.string.vegetables))){
                            ivCategory.setImageResource(R.mipmap.vegetables);
                        }
                        if(selectedCategory.equals(getString(R.string.dessert))){
                            ivCategory.setImageResource(R.mipmap.sweets);
                        }
                        if(selectedCategory.equals(getString(R.string.dip))){
                            ivCategory.setImageResource(R.mipmap.dipping);
                        }
                        if(selectedCategory.equals(getString(R.string.salad))){
                            ivCategory.setImageResource(R.mipmap.salat);
                        }
                        if(selectedCategory.equals(getString(R.string.pasta))){
                            ivCategory.setImageResource(R.mipmap.spaghetti);
                        }
                    }
                    btn_add_edit_MealName.setText(getString(R.string.Add_Meal_btn_editMealName));
                }
                break;
            case REQUEST_CODE_GET_INGREDIENT:
                if(resultCode == Activity.RESULT_OK){
                    String ingredientName = data.getStringExtra("ingrName");
                    String ingredientAmount = data.getStringExtra("ingrAmount");
                    String ingredientUnit = data.getStringExtra("ingrUnit");
                    ingredients.add(new Ingredient(ingredientName,ingredientAmount,ingredientUnit));
                    ingredientAdapter.notifyDataSetChanged();
                }
                break;
        }
    }
}