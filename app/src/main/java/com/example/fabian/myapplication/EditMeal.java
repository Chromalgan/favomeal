package com.example.fabian.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fabian.myapplication.appData.Ingredient;
import com.example.fabian.myapplication.appData.IngredientAdapter;
import com.example.fabian.myapplication.appData.Meal;
import com.example.fabian.myapplication.popup_windows.PopUpWindowAddIngredient;
import com.example.fabian.myapplication.popup_windows.PopUpWindowAddName;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Class EditMeal, which represents the Activity to edit a existing meal
 */
public class EditMeal extends AppCompatActivity {

    public static final int REQUEST_CODE_GET_MEAL_NAME = 101;
    public static final int REQUEST_CODE_GET_INGREDIENT = 102;
    private ArrayList<Ingredient> ingredients;
    String selectedCategory;
    Button btn_add_edit_MealName, btn_add_Ingredient, btn_save_Meal;
    TextView tv_MealName, tv_persons;
    ImageView ivCategory;
    RecyclerView rv_Ingredients;
    IngredientAdapter ingredientAdapter;
    Intent intent;
    private int mealIndex;
    Meal mealToEdit;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_meal);

        tv_MealName = findViewById(R.id.textView_editMeal_MealName);
        ivCategory = findViewById(R.id.editMeal_ivCategoryIcon);
        tv_persons = findViewById(R.id.tv_editmeal_Persons);

        btn_save_Meal =  findViewById(R.id.btn_editMeal_saveMeal);
        btn_add_edit_MealName = findViewById(R.id.btn_editMeal_addName);
        btn_add_Ingredient = findViewById(R.id.btn_editMeal_addIngredient);

        if(getIntent() != null && getIntent().getStringExtra("intentFromMyMealsDetail") != null){
            mealIndex = getIntent().getIntExtra("mealIndex",999);
            Gson gson = new Gson();
            String json = getIntent().getStringExtra("mealToEdit");
            Type type = new TypeToken<Meal>(){}.getType();
            mealToEdit = gson.fromJson(json, type);
            ingredients = mealToEdit.getIngredients();
            tv_MealName.setText(mealToEdit.getMealName());
            selectedCategory = mealToEdit.getMealCategory();
            String personAttach = mealToEdit.getMealPersons() +" "+ getString(R.string.attachment_persons_short);
            tv_persons.setText(personAttach);
            drawCategoryIcon();
            buildRecyclerView();
        }
        btn_add_edit_MealName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), PopUpWindowAddName.class);
                startActivityForResult(intent, REQUEST_CODE_GET_MEAL_NAME);
            }
        });
        btn_add_Ingredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), PopUpWindowAddIngredient.class);
                startActivityForResult(intent, REQUEST_CODE_GET_INGREDIENT);
            }
        });
        btn_save_Meal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ingredients.size() == 0 || tv_MealName.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.addMeal_saveMeal_NoIngredients), Toast.LENGTH_SHORT).show();
                } else {
                    intent = new Intent(getApplicationContext(), MyMealsDetail.class);
                    mealToEdit.setIngredients(ingredients);
                    Gson gson = new Gson();
                    String editedMeal = gson.toJson(mealToEdit);
                    intent.putExtra("selectedMeal", editedMeal);
                    intent.putExtra("intentFromEdit", "flag");
                    intent.putExtra("mealIndex", mealIndex);
                    startActivity(intent);
                }
            }
        });
    }

    /**
     * method to react on Result intents
     *
     * @param requestCode requestcode of incoming intent
     * @param resultCode code to show if received intent is ok
     * @param data data of intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode){
            case REQUEST_CODE_GET_MEAL_NAME:
                if(resultCode == Activity.RESULT_OK){
                    tv_MealName.setText(data.getStringExtra("mealName"));
                    mealToEdit.setMealName(data.getStringExtra("mealName"));
                    selectedCategory = data.getStringExtra("mealCategory");
                    if(selectedCategory != null){
                        if(selectedCategory.equals(getString(R.string.stew))){
                            selectedCategory = getString(R.string.categoryStew);
                        }
                        if(selectedCategory.equals(getString(R.string.fish))){
                            selectedCategory = getString(R.string.categoryFish);
                        }
                        if(selectedCategory.equals(getString(R.string.meat))){
                            selectedCategory = getString(R.string.categoryMeat);
                        }
                        if(selectedCategory.equals(getString(R.string.vegetables))){
                            selectedCategory = getString(R.string.categoryVegetables);
                        }
                        if(selectedCategory.equals(getString(R.string.dessert))){
                            selectedCategory = getString(R.string.categoryDessert);
                        }
                        if(selectedCategory.equals(getString(R.string.dip))){
                            selectedCategory = getString(R.string.categoryDip);
                        }
                        if(selectedCategory.equals(getString(R.string.salad))){
                            selectedCategory = getString(R.string.categorySalad);
                        }
                        if(selectedCategory.equals(getString(R.string.pasta))){
                            selectedCategory = getString(R.string.categoryPasta);
                        }
                    }
                    mealToEdit.setMealCategory(selectedCategory);
                    String personAttach = data.getStringExtra("mealPersons") +"  "+ getString(R.string.attachment_persons);
                    mealToEdit.setMealPersons(data.getStringExtra("mealPersons"));
                    tv_persons.setText(personAttach);
                    drawCategoryIcon();
                    btn_add_edit_MealName.setText(getString(R.string.Add_Meal_btn_editMealName));
                }
                break;
            case REQUEST_CODE_GET_INGREDIENT:
                if(resultCode == Activity.RESULT_OK){
                    String ingredientName = data.getStringExtra("ingrName");
                    String ingredientAmount = data.getStringExtra("ingrAmount");
                    String ingredientUnit = data.getStringExtra("ingrUnit");
                    ingredients.add(new Ingredient(ingredientName,ingredientAmount,ingredientUnit));
                    ingredientAdapter.notifyDataSetChanged();
                }
                break;
        }
    }

    /**
     * Method to build the recyclerView at Activity start
     */
    private void buildRecyclerView() {
        rv_Ingredients = findViewById(R.id.rv_EditMeal_Ingredients);
        rv_Ingredients.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        ingredientAdapter = new IngredientAdapter(getApplicationContext(),ingredients, true, false, null);
        rv_Ingredients.setAdapter(ingredientAdapter);
    }

    private void drawCategoryIcon(){
        if(selectedCategory != null){
            if(selectedCategory.equals(getString(R.string.categoryStew))){
                ivCategory.setImageResource(R.mipmap.eintopf);
            }
            if(selectedCategory.equals(getString(R.string.categoryFish))){
                ivCategory.setImageResource(R.mipmap.fish);
            }
            if(selectedCategory.equals(getString(R.string.categoryMeat))){
                ivCategory.setImageResource(R.mipmap.cow);
            }
            if(selectedCategory.equals(getString(R.string.categoryVegetables))){
                ivCategory.setImageResource(R.mipmap.vegetables);
            }
            if(selectedCategory.equals(getString(R.string.categoryDessert))){
                ivCategory.setImageResource(R.mipmap.sweets);
            }
            if(selectedCategory.equals(getString(R.string.categoryDip))){
                ivCategory.setImageResource(R.mipmap.dipping);
            }
            if(selectedCategory.equals(getString(R.string.categorySalad))){
                ivCategory.setImageResource(R.mipmap.salat);
            }
            if(selectedCategory.equals(getString(R.string.categoryPasta))){
                ivCategory.setImageResource(R.mipmap.spaghetti);
            }
        }
    }
}
