package com.example.fabian.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Class MainActivity, which represents the Activity at activity start and the main menu
 */
public class MainActivity extends AppCompatActivity {

    Button btnMyMeals, btnMyList, btnMyStats;
    Intent intent;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMyMeals =  findViewById(R.id.btn_MyMeals);
        btnMyList =  findViewById(R.id.btn_MyList);
        btnMyStats =  findViewById(R.id.btn_MyStats);

        btnMyMeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), MyMeals.class);
                startActivity(intent);
            }
        });
        btnMyList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), MyLists.class);
                startActivity(intent);
            }
        });
        btnMyStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), MyHistory.class);
                startActivity(intent);
            }
        });
    }
}
