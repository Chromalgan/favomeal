package com.example.fabian.myapplication;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.fabian.myapplication.appData.HistoryList;
import com.example.fabian.myapplication.appData.HistoryListAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Class MyHistory, which represents the Activity to list the history meals
 */
public class MyHistory extends AppCompatActivity {

    public static final String PREFS_NAME_HISTORY = "MY_APP_STORAGE_HISTORY";
    public static final String FAVORITE_HISTORY = "MyHistory";
    ArrayList<HistoryList> historyLists;
    private HistoryListAdapter historyListAdapter;
    private RecyclerView myHistoryView;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_history);
        loadData();
        buildRecyclerView();
    }

    /**
     * Method to build the recyclerView at Activity start
     */
    private void buildRecyclerView() {
        myHistoryView = findViewById(R.id.myHistoryRecyclerView);
        myHistoryView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        historyListAdapter = new HistoryListAdapter(getApplicationContext(),historyLists);
        myHistoryView.setAdapter(historyListAdapter);
    }
    /**
     * Method to load Shared Preferences at Activity Load and store content in Arraylist
     */
    private void loadData(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME_HISTORY, MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString(FAVORITE_HISTORY,null);
        Type type = new TypeToken<ArrayList<HistoryList>>(){}.getType();
        historyLists = gson.fromJson(json, type);
        if(historyLists == null){
            historyLists = new ArrayList<>();
        }
    }
}
