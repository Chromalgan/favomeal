package com.example.fabian.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Class MyHistoryDetail, which represents the Activity as popup to show history meal detail
 */
public class MyHistoryDetail extends AppCompatActivity {

    TextView tv_historyMeal_name;
    TextView tv_historyMeal_persons;
    TextView tv_historyMeal_time;
    TextView tv_historyMeal_comment;
    ImageView iv_historyMeal_icon;
    int width, height;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_history_detail);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        width = dm.widthPixels;
        height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.9), (int) (height*0.5));

        tv_historyMeal_name = findViewById(R.id.tv_historyDetail_name);
        tv_historyMeal_persons = findViewById(R.id.tv_historyDetail_persons);
        tv_historyMeal_time = findViewById(R.id.tv_historyDetail_time);
        tv_historyMeal_comment = findViewById(R.id.tv_historyDetail_comment);
        iv_historyMeal_icon = findViewById(R.id.iv_historyDetail_icon);

        if(getIntent() != null) {
            tv_historyMeal_name.setText(getIntent().getStringExtra("historyMealName"));
            tv_historyMeal_persons.setText(getIntent().getStringExtra("historyMealPersons"));
            tv_historyMeal_time.setText(getIntent().getStringExtra("historyMealDate"));
            tv_historyMeal_comment.setText(getIntent().getStringExtra("historyMealComment"));
            String selectedCategory = getIntent().getStringExtra("historyMealCategory");
            if(selectedCategory != null){
                if(selectedCategory.equals(getString(R.string.categoryStew))){
                    iv_historyMeal_icon.setImageResource(R.mipmap.eintopf);
                }
                if(selectedCategory.equals(getString(R.string.categoryFish))){
                    iv_historyMeal_icon.setImageResource(R.mipmap.fish);
                }
                if(selectedCategory.equals(getString(R.string.categoryMeat))){
                    iv_historyMeal_icon.setImageResource(R.mipmap.cow);
                }
                if(selectedCategory.equals(getString(R.string.categoryVegetables))){
                    iv_historyMeal_icon.setImageResource(R.mipmap.vegetables);
                }
                if(selectedCategory.equals(getString(R.string.categoryDessert))){
                    iv_historyMeal_icon.setImageResource(R.mipmap.sweets);
                }
                if(selectedCategory.equals(getString(R.string.categoryDip))){
                    iv_historyMeal_icon.setImageResource(R.mipmap.dipping);
                }
                if(selectedCategory.equals(getString(R.string.categorySalad))){
                    iv_historyMeal_icon.setImageResource(R.mipmap.salat);
                }
                if(selectedCategory.equals(getString(R.string.categoryPasta))){
                    iv_historyMeal_icon.setImageResource(R.mipmap.spaghetti);
                }
            }
        }
    }
}
