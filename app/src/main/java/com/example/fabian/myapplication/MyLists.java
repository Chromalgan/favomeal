package com.example.fabian.myapplication;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.fabian.myapplication.appData.ShopList;
import com.example.fabian.myapplication.appData.ShopListAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Class MyLists, which represents the Activity to list all shoppinglists
 */
public class MyLists extends AppCompatActivity {

    public static final String PREFS_NAME_LIST = "MY_APP_STORAGE_LIST";
    public static final String FAVORITE_LISTS = "MyLists";
    ArrayList<ShopList> shoppingLists;
    private ShopListAdapter listAdapter;
    private RecyclerView myListsView;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_lists);
        loadData();
        buildRecyclerView();
    }

    /**
     * Method to build the recyclerView at Activity start
     */
    private void buildRecyclerView() {
        myListsView = findViewById(R.id.rv_MyLists);
        myListsView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        listAdapter = new ShopListAdapter(getApplicationContext(),shoppingLists);
        myListsView.setAdapter(listAdapter);
    }
    /**
     * Method to load Shared Preferences at Activity Load and store content in Arraylist
     */
    private void loadData(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME_LIST, MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString(FAVORITE_LISTS,null);
        Type type = new TypeToken<ArrayList<ShopList>>(){}.getType();
        shoppingLists = gson.fromJson(json, type);

        if(shoppingLists == null){
            shoppingLists = new ArrayList<>();
        }
    }
}
