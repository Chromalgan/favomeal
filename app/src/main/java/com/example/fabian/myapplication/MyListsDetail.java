package com.example.fabian.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fabian.myapplication.appData.Ingredient;
import com.example.fabian.myapplication.appData.IngredientAdapter;
import com.example.fabian.myapplication.appData.Meal;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Class MyListsDetail, which represents the Activity to show detail view of a shoppinglist
 */
public class MyListsDetail extends AppCompatActivity {

    Intent intent;
    Meal selectedMeal;
    ArrayList<Ingredient> selectedIngredients;
    IngredientAdapter ingredientAdapter;
    TextView tv_List_MealName, tv_List_MealPersons;
    ImageView iv_List_MealCategory;
    RecyclerView ingredientsView;
    Button btn_startShop;
    String personsForList;
    int listIndex;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_lists_detail);

        tv_List_MealName = findViewById(R.id.listDetail_name);
        iv_List_MealCategory = findViewById(R.id.listDetail_category);
        tv_List_MealPersons = findViewById(R.id.listDetail_persons);
        btn_startShop = findViewById(R.id.listDetail_btn_start);

        if(getIntent() != null) {
            Gson gson = new Gson();
            String json = getIntent().getStringExtra("selectedList");
            listIndex = getIntent().getIntExtra("listIndex", 999);
            Type type = new TypeToken<Meal>() {
            }.getType();
            selectedMeal = gson.fromJson(json, type);
            selectedIngredients = selectedMeal.getIngredients();
            tv_List_MealName.setText(selectedMeal.getMealName());
            personsForList = getIntent().getStringExtra("selectedListPersons");
            String personAttach = personsForList + "  " + getString(R.string.attachment_persons);
            tv_List_MealPersons.setText(personAttach);
            String selectedCategory = selectedMeal.getMealCategory();
            if(selectedCategory != null){
                if(selectedCategory.equals(getString(R.string.categoryStew))){
                    iv_List_MealCategory.setImageResource(R.mipmap.eintopf);
                }
                if(selectedCategory.equals(getString(R.string.categoryFish))){
                    iv_List_MealCategory.setImageResource(R.mipmap.fish);
                }
                if(selectedCategory.equals(getString(R.string.categoryMeat))){
                    iv_List_MealCategory.setImageResource(R.mipmap.cow);
                }
                if(selectedCategory.equals(getString(R.string.categoryVegetables))){
                    iv_List_MealCategory.setImageResource(R.mipmap.vegetables);
                }
                if(selectedCategory.equals(getString(R.string.categoryDessert))){
                    iv_List_MealCategory.setImageResource(R.mipmap.sweets);
                }
                if(selectedCategory.equals(getString(R.string.categoryDip))){
                    iv_List_MealCategory.setImageResource(R.mipmap.dipping);
                }
                if(selectedCategory.equals(getString(R.string.categorySalad))){
                    iv_List_MealCategory.setImageResource(R.mipmap.salat);
                }
                if(selectedCategory.equals(getString(R.string.categoryPasta))){
                    iv_List_MealCategory.setImageResource(R.mipmap.spaghetti);
                }
            }
            buildRecyclerView();
        }
        btn_startShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), MyListsShoppingList.class);
                Gson gson = new Gson();
                String meal = gson.toJson(selectedMeal);
                intent.putExtra("selectedList", meal);
                intent.putExtra("selectedListPersons", personsForList);
                intent.putExtra("intentFromMyListsDetail", "intentFlag");
                intent.putExtra("listIndex", listIndex);
                view.getContext().startActivity(intent);
                startActivity(intent);
            }
        });
    }
    /**
     * Method to build the recyclerView at Activity start
     */
    private void buildRecyclerView() {
        ingredientsView = findViewById(R.id.listDetail_rv);
        ingredientsView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        ingredientAdapter = new IngredientAdapter(getApplicationContext(),selectedIngredients, false, false, null);
        ingredientsView.setAdapter(ingredientAdapter);
    }
}
