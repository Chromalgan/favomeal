package com.example.fabian.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fabian.myapplication.appData.HistoryList;
import com.example.fabian.myapplication.appData.Ingredient;
import com.example.fabian.myapplication.appData.IngredientAdapter;
import com.example.fabian.myapplication.appData.Meal;
import com.example.fabian.myapplication.appData.ShopList;
import com.example.fabian.myapplication.popup_windows.PopUpWindowFinishShopList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Class MyListsShoppingList, which represents the Activity checkk ingredients of shoppinglist
 */
public class MyListsShoppingList extends AppCompatActivity {

    final int REQUEST_CODE_FINISH_SHOPPING = 110;
    public static final String PREFS_NAME_HISTORY = "MY_APP_STORAGE_HISTORY";
    public static final String FAVORITE_HISTORY = "MyHistory";
    public static final String PREFS_NAME_LIST = "MY_APP_STORAGE_LIST";
    public static final String FAVORITE_LISTS = "MyLists";
    ArrayList<Ingredient> selectedIngredients;
    ArrayList<HistoryList> historyLists;
    ArrayList<ShopList> shoppingLists;
    TextView tv_shopList_MealName, tv_shopList_MealPersons;
    ImageView iv_shopList_MealCategory;
    RecyclerView ingredientsViewShopList;
    IngredientAdapter ingredientAdapterShopList;
    Button btn_finishShop;
    Meal selectedMeal;
    Intent intent;
    String mealPersons;
    Date currentTime;
    private int listIndex;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_lists_shopping_list);

        tv_shopList_MealName = findViewById(R.id.tv_shopList_name);
        tv_shopList_MealPersons = findViewById(R.id.tv_shopList_persons);
        iv_shopList_MealCategory = findViewById(R.id.iv_shopList);
        btn_finishShop = findViewById(R.id.btn_shopList_finish);
        // disable button at activity start
        btn_finishShop.setBackgroundResource(R.drawable.button_rounded_corners_disabled);
        btn_finishShop.setEnabled(false);

        if(getIntent() != null) {
            Gson gson = new Gson();
            String json = getIntent().getStringExtra("selectedList");
            listIndex = getIntent().getIntExtra("listIndex",999);
            Type type = new TypeToken<Meal>() {
            }.getType();
            selectedMeal = gson.fromJson(json, type);
            selectedIngredients = selectedMeal.getIngredients();
            tv_shopList_MealName.setText(selectedMeal.getMealName());
            mealPersons = getIntent().getStringExtra("selectedListPersons");
            String personAttach = getIntent().getStringExtra("selectedListPersons") + "  " + getString(R.string.attachment_persons);
            tv_shopList_MealPersons.setText(personAttach);
            String selectedCategory = selectedMeal.getMealCategory();
            if(selectedCategory != null){
                if(selectedCategory.equals(getString(R.string.categoryStew))){
                    iv_shopList_MealCategory.setImageResource(R.mipmap.eintopf);
                }
                if(selectedCategory.equals(getString(R.string.categoryFish))){
                    iv_shopList_MealCategory.setImageResource(R.mipmap.fish);
                }
                if(selectedCategory.equals(getString(R.string.categoryMeat))){
                    iv_shopList_MealCategory.setImageResource(R.mipmap.cow);
                }
                if(selectedCategory.equals(getString(R.string.categoryVegetables))){
                    iv_shopList_MealCategory.setImageResource(R.mipmap.vegetables);
                }
                if(selectedCategory.equals(getString(R.string.categoryDessert))){
                    iv_shopList_MealCategory.setImageResource(R.mipmap.sweets);
                }
                if(selectedCategory.equals(getString(R.string.categoryDip))){
                    iv_shopList_MealCategory.setImageResource(R.mipmap.dipping);
                }
                if(selectedCategory.equals(getString(R.string.categorySalad))){
                    iv_shopList_MealCategory.setImageResource(R.mipmap.salat);
                }
                if(selectedCategory.equals(getString(R.string.categoryPasta))){
                    iv_shopList_MealCategory.setImageResource(R.mipmap.spaghetti);
                }
            }
            buildRecyclerView();
        }
        btn_finishShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), PopUpWindowFinishShopList.class);
                intent.putExtra("listIndex", listIndex);
                startActivityForResult(intent, REQUEST_CODE_FINISH_SHOPPING);
            }
        });
    }
    /**
     * method to react on Result intents
     *
     * @param requestCode requestcode of incoming intent
     * @param resultCode code to show if received intent is ok
     * @param data data of intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode){
            case REQUEST_CODE_FINISH_SHOPPING:
                if(resultCode == Activity.RESULT_OK){
                    //save list to history
                    loadMyHistoryData();
                    String comment = data.getStringExtra("listComment");
                    currentTime = Calendar.getInstance().getTime();
                    HistoryList historyList = new HistoryList(selectedMeal.getMealName(), mealPersons, selectedMeal.getMealCategory(), comment, currentTime);
                    historyLists.add(historyList);
                    saveMyHistoryData();
                    //delete shopping list item from shopping list
                    loadMyListData();
                    shoppingLists.remove(listIndex);
                    saveMyListData();
                    //go back to MyListz
                    intent = new Intent(getApplicationContext(), MyLists.class);
                    startActivity(intent);
                    //Toast that shopping is finished
                    Toast.makeText(getApplicationContext(), getString(R.string.myList_finishList_Success), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    /**
     * Method to build the recyclerView at Activity start
     */
    private void buildRecyclerView() {
        ingredientsViewShopList = findViewById(R.id.rv_shopList);
        ingredientsViewShopList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        ingredientAdapterShopList = new IngredientAdapter(getApplicationContext(),selectedIngredients, false, true, btn_finishShop);
        ingredientsViewShopList.setAdapter(ingredientAdapterShopList);
    }

    /**
     * Method to load MyHistories from Shared Preferences at Activity load and store content in ArrayList
     */
    private void loadMyHistoryData(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME_HISTORY, MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString(FAVORITE_HISTORY,null);
        Type type = new TypeToken<ArrayList<HistoryList>>(){}.getType();
        historyLists = gson.fromJson(json, type);
        if(historyLists == null){
            historyLists = new ArrayList<>();
        }
    }

    /**
     * Method to save Arraylist<HistoryList> to Shared Preferences
     */
    private void saveMyHistoryData() {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME_HISTORY, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(historyLists);
        editor.putString(FAVORITE_HISTORY, json);
        editor.apply();
    }
    /**
     * Method to load MyHistories from Shared Preferences at Activity load and store content in ArrayList
     */
    private void loadMyListData(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME_LIST, MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString(FAVORITE_LISTS,null);
        Type type = new TypeToken<ArrayList<ShopList>>(){}.getType();
        shoppingLists = gson.fromJson(json, type);
        if(shoppingLists == null){
            shoppingLists = new ArrayList<>();
        }
    }

    /**
     * Method to save Arraylist<HistoryList> to Shared Preferences
     */
    private void saveMyListData() {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME_LIST, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(shoppingLists);
        editor.putString(FAVORITE_LISTS, json);
        editor.apply();
    }
}
