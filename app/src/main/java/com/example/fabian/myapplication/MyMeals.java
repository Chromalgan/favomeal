package com.example.fabian.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.fabian.myapplication.appData.Ingredient;
import com.example.fabian.myapplication.appData.Meal;
import com.example.fabian.myapplication.appData.MealAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Class MyMeals, which represents the Activity to show all stored meals
 */
public class MyMeals extends AppCompatActivity {

    ArrayList<Meal> meals;
    ArrayList<Ingredient> newIngredients;
    private MealAdapter mealAdapter;
    private String newMealName, newMealCategory, newMealPersons;
    private Bundle args;
    private Button btnAddMeal;
    private Intent intent;
    private RecyclerView myMealsView;
    public static final String PREFS_NAME = "MY_APP_STORAGE";
    public static final String FAVORITE_MEALS = "MyMeals";

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_meals);

        loadData();
        buildRecyclerView();
        if(getIntent() != null && getIntent().getStringExtra("intentFromAddMeal") != null){
            intent = getIntent();
            args = intent.getBundleExtra("newMealIngredients");
            newIngredients = (ArrayList<Ingredient>) args.getSerializable("INGREDIENTS");
            newMealName = intent.getStringExtra("newMealName");
            newMealCategory = intent.getStringExtra("newMealCategory");
            newMealPersons = intent.getStringExtra("newMealPersons");
            if(newMealCategory != null){
                if(newMealCategory.equals(getString(R.string.stew))){
                    newMealCategory = getString(R.string.categoryStew);
                }
                if(newMealCategory.equals(getString(R.string.fish))){
                    newMealCategory = getString(R.string.categoryFish);
                }
                if(newMealCategory.equals(getString(R.string.meat))){
                    newMealCategory = getString(R.string.categoryMeat);
                }
                if(newMealCategory.equals(getString(R.string.vegetables))){
                    newMealCategory = getString(R.string.categoryVegetables);
                }
                if(newMealCategory.equals(getString(R.string.dessert))){
                    newMealCategory = getString(R.string.categoryDessert);
                }
                if(newMealCategory.equals(getString(R.string.dip))){
                    newMealCategory = getString(R.string.categoryDip);
                }
                if(newMealCategory.equals(getString(R.string.salad))){
                    newMealCategory = getString(R.string.categorySalad);
                }
                if(newMealCategory.equals(getString(R.string.pasta))){
                    newMealCategory = getString(R.string.categoryPasta);
                }
            }
            Meal newMeal = new Meal(newMealName, newMealCategory, newIngredients, newMealPersons);
            meals.add(newMeal);
            mealAdapter.notifyDataSetChanged();
            saveData();
            Toast.makeText(getApplicationContext(), getString(R.string.addMeal_saveMeal_Success), Toast.LENGTH_SHORT).show();
        }
        btnAddMeal = findViewById(R.id.btn_MyMeals_add);
        btnAddMeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), AddMeal.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Method to build the recyclerView at Activity start
     */
    private void buildRecyclerView() {
        myMealsView = findViewById(R.id.myMealsRecyclerView);
        myMealsView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mealAdapter = new MealAdapter(getApplicationContext(),meals);
        myMealsView.setAdapter(mealAdapter);
    }

    /**
     * Method to save Arraylist<Meal> to Shared Preferences
     */
    private void saveData() {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(meals);
        editor.putString(FAVORITE_MEALS, json);
        editor.apply();
    }

    /**
     * Method to load Shared Preferences at Activity Load and store content in Arraylist
     */
    private void loadData(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        //prefs.edit().clear().apply();
        Gson gson = new Gson();
        String json = prefs.getString(FAVORITE_MEALS,null);
        Type type = new TypeToken<ArrayList<Meal>>(){}.getType();
        meals = gson.fromJson(json, type);

        if(meals == null){
            meals = new ArrayList<>();
        }
    }
}
