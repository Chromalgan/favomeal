package com.example.fabian.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fabian.myapplication.appData.Ingredient;
import com.example.fabian.myapplication.appData.IngredientAdapter;
import com.example.fabian.myapplication.appData.Meal;
import com.example.fabian.myapplication.appData.ShopList;
import com.example.fabian.myapplication.popup_windows.PopUpWindowGenerateShopList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Class MyMealsDetail, which represents the Activity to show detail view of a meal
 */
public class MyMealsDetail extends AppCompatActivity {

    public static final String PREFS_NAME = "MY_APP_STORAGE";
    public static final String PREFS_NAME_LIST = "MY_APP_STORAGE_LIST";
    public static final String FAVORITE_MEALS = "MyMeals";
    public static final String FAVORITE_LISTS = "MyLists";
    public static final int REQUEST_CODE_GET_PERSONS_FOR_LIST = 105;
    private int mealIndex;
    private float listPersonsNumber;
    private float tempAmount;
    private float defaultPersons;
    private float amountFactorToCalculate;
    private float newCalculatedAmount;
    Intent intent;
    Meal selectedMeal;
    Meal shopListMeal;
    ArrayList<Meal> meals;
    ArrayList<ShopList> shoppingLists;
    ArrayList<Ingredient> selectedIngredients;
    ArrayList<Ingredient> listIngredientsToCalculate;
    IngredientAdapter ingredientAdapter;
    TextView tv_MealName, tv_MealPersons;
    ImageView iv_MealCategory;
    ImageView iv_MealEdit;
    RecyclerView ingredientsView;
    Button generateShoppingList;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_meals_detail);

        tv_MealName = findViewById(R.id.tv_Meals_Detail);
        iv_MealCategory = findViewById(R.id.iv_Meals_Detail);
        iv_MealEdit = findViewById(R.id.iv_Edit_Meal);
        tv_MealPersons = findViewById(R.id.tv_Detail_Persons);
        generateShoppingList = findViewById(R.id.btn_Meals_Detail_List);

        if(getIntent() != null){
            mealIndex = getIntent().getIntExtra("mealIndex",999);
        }
        buildActivityFromMealData();

        if(getIntent().getStringExtra("intentFromEdit") != null){
            loadMyMealsData();
            meals.remove(mealIndex);
            meals.add(mealIndex, selectedMeal);
            saveMyMealsData();
            Toast.makeText(getApplicationContext(), getString(R.string.addMeal_editMeal_Success), Toast.LENGTH_SHORT).show();
        }

        iv_MealEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), EditMeal.class);
                Gson gson = new Gson();
                String meal = gson.toJson(selectedMeal);
                intent.putExtra("mealToEdit", meal);
                intent.putExtra("intentFromMyMealsDetail", "intentFlag");
                intent.putExtra("mealIndex", mealIndex);
                startActivity(intent);
            }
        });
        generateShoppingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), PopUpWindowGenerateShopList.class);
                startActivityForResult(intent, REQUEST_CODE_GET_PERSONS_FOR_LIST);
            }
        });
    }

    /**
     * method to react on Result intents
     *
     * @param requestCode requestcode of incoming intent
     * @param resultCode code to show if received intent is ok
     * @param data data of intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode){
            case REQUEST_CODE_GET_PERSONS_FOR_LIST:
                if(resultCode == Activity.RESULT_OK){
                    loadMyListsData();
                    String selectedPersons = data.getStringExtra("personsForList");
                    try {
                        shopListMeal = (Meal) selectedMeal.clone();
                        listPersonsNumber = Float.parseFloat(selectedPersons);
                        defaultPersons = Float.parseFloat(selectedMeal.getMealPersons());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(shopListMeal != null && listPersonsNumber != 0 && defaultPersons != 0){
                        listIngredientsToCalculate = shopListMeal.getIngredients();
                        amountFactorToCalculate = listPersonsNumber/defaultPersons;

                        for (Ingredient ingredientItem: listIngredientsToCalculate) {
                            tempAmount = Float.parseFloat(ingredientItem.getAmount());
                            newCalculatedAmount = tempAmount*amountFactorToCalculate;
                            newCalculatedAmount = (float)(((int)(newCalculatedAmount*10))/10.0);
                            ingredientItem.setAmount(String.valueOf(newCalculatedAmount));
                        }
                        shopListMeal.setIngredients(listIngredientsToCalculate);
                        ShopList shopList = new ShopList(shopListMeal, selectedPersons);
                        shoppingLists.add(shopList);
                        saveMyListsData();
                    }
                    Toast.makeText(getApplicationContext(), getString(R.string.addList_saveList_Success), Toast.LENGTH_SHORT).show();
                    intent = new Intent(getApplicationContext(), MyLists.class);
                    startActivity(intent);
                }
                break;
        }
    }

    /**
     * Method to build the recyclerView at Activity start
     */
    private void buildRecyclerView() {
        ingredientsView = findViewById(R.id.rv_Meals_Detail);
        ingredientsView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        ingredientAdapter = new IngredientAdapter(getApplicationContext(),selectedIngredients, false, false, null);
        ingredientsView.setAdapter(ingredientAdapter);
    }
    /**
     * Method to load MyMeals from Shared Preferences at Activity load and store content in Arraylist
     */
    private void loadMyMealsData(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString(FAVORITE_MEALS,null);
        Type type = new TypeToken<ArrayList<Meal>>(){}.getType();
        meals = gson.fromJson(json, type);

        if(meals == null){
            meals = new ArrayList<>();
        }
    }
    /**
     * Method to save Arraylist<Meal> to Shared Preferences
     */
    private void saveMyMealsData() {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(meals);
        editor.putString(FAVORITE_MEALS, json);
        editor.apply();
    }
    /**
     * Method to load MyMeals from Shared Preferences at Activity load and store content in Arraylist
     */
    private void loadMyListsData(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME_LIST, MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString(FAVORITE_LISTS,null);
        Type type = new TypeToken<ArrayList<ShopList>>(){}.getType();
        shoppingLists = gson.fromJson(json, type);

        if(shoppingLists == null){
            shoppingLists = new ArrayList<>();
        }
    }
    /**
     * Method to save Arraylist<Meal> to Shared Preferences
     */
    private void saveMyListsData() {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME_LIST, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(shoppingLists);
        editor.putString(FAVORITE_LISTS, json);
        editor.apply();
    }

    /**
     * method to fill recyclerView card elements with data
     */
    private void buildActivityFromMealData(){
        Gson gson = new Gson();
        String json = getIntent().getStringExtra("selectedMeal");
        Type type = new TypeToken<Meal>(){}.getType();
        selectedMeal = gson.fromJson(json, type);
        selectedIngredients = selectedMeal.getIngredients();
        tv_MealName.setText(selectedMeal.getMealName());
        String personAttach = selectedMeal.getMealPersons() +"  "+ getString(R.string.attachment_persons);
        tv_MealPersons.setText(personAttach);
        if(selectedMeal.getMealCategory() != null){
            if(selectedMeal.getMealCategory().equals(getString(R.string.categoryStew))){
                iv_MealCategory.setImageResource(R.mipmap.eintopf);
            }
            if(selectedMeal.getMealCategory().equals(getString(R.string.categoryFish))){
                iv_MealCategory.setImageResource(R.mipmap.fish);
            }
            if(selectedMeal.getMealCategory().equals(getString(R.string.categoryMeat))){
                iv_MealCategory.setImageResource(R.mipmap.cow);
            }
            if(selectedMeal.getMealCategory().equals(getString(R.string.categoryVegetables))){
                iv_MealCategory.setImageResource(R.mipmap.vegetables);
            }
            if(selectedMeal.getMealCategory().equals(getString(R.string.categoryVegetables))){
                iv_MealCategory.setImageResource(R.mipmap.sweets);
            }
            if(selectedMeal.getMealCategory().equals(getString(R.string.categoryDip))){
                iv_MealCategory.setImageResource(R.mipmap.dipping);
            }
            if(selectedMeal.getMealCategory().equals(getString(R.string.categorySalad))){
                iv_MealCategory.setImageResource(R.mipmap.salat);
            }
            if(selectedMeal.getMealCategory().equals(getString(R.string.categoryPasta))){
                iv_MealCategory.setImageResource(R.mipmap.spaghetti);
            }
        }
        buildRecyclerView();
    }
}
