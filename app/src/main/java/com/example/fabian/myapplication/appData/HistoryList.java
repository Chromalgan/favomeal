package com.example.fabian.myapplication.appData;

import java.util.Date;

/**
 * Class HistoryList, which represents a POJO class for History meals
 */
public class HistoryList {

    String mealName;
    String persons;
    String category;
    String comment;
    Date date;

    /**
     * constructor
     *
     * @param mealName name of meal
     * @param persons persons for meal
     * @param category category of meal
     * @param comment otional comment for meal
     * @param date date of finishing the shoppinglist
     */
    public HistoryList(String mealName, String persons, String category, String comment, Date date) {
        this.mealName = mealName;
        this.persons = persons;
        this.category = category;
        this.comment = comment;
        this.date = date;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public String getPersons() {
        return persons;
    }

    public void setPersons(String persons) {
        this.persons = persons;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
