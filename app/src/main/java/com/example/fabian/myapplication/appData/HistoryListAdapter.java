package com.example.fabian.myapplication.appData;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fabian.myapplication.MyHistoryDetail;
import com.example.fabian.myapplication.R;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * adapter class HistoryListAdapter for recyclerView to fill it with content
 */
public class HistoryListAdapter extends RecyclerView.Adapter {

    ArrayList<HistoryList> historyLists;
    private Context mContext;
    public static final String PREFS_NAME_HISTORY = "MY_APP_STORAGE_HISTORY";
    public static final String FAVORITE_HISTORY = "MyHistory";
    DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm");

    /**
     * constructor
     *
     * @param context context
     * @param historyLists list of history meals
     */
    public HistoryListAdapter(Context context, ArrayList<HistoryList> historyLists) {
        this.mContext = context;
        this.historyLists = historyLists;
    }

    /**
     * class CardViewHolderHistory to to fill card elements with data
     */
    public static class CardViewHolderHistory extends RecyclerView.ViewHolder{
        public ImageView deleteIcon;
        public ImageView mealImage;
        public ImageView commentImage;
        public TextView mealName;

        public CardViewHolderHistory(View view) {
            super(view);
            deleteIcon = view.findViewById(R.id.historyCard_deleteIcon);
            mealImage = view.findViewById(R.id.historyCard_Image);
            mealName = view.findViewById(R.id.tv_historyCard_name);
            commentImage = view.findViewById(R.id.historyCard_commentImage);
        }

        /**
         * set data in card
         * @param list one history meal
         * @param mContext context
         */
        public void setHistoryData(HistoryList list, Context mContext) {
            mealName.setText(list.getMealName());
            if(list.getCategory() != null){
                if(list.getCategory().equals(mContext.getString(R.string.categoryStew))){
                    mealImage.setImageResource(R.mipmap.eintopf);
                }
                if(list.getCategory().equals(mContext.getString(R.string.categoryFish))){
                    mealImage.setImageResource(R.mipmap.fish);
                }
                if(list.getCategory().equals(mContext.getString(R.string.categoryMeat))){
                    mealImage.setImageResource(R.mipmap.cow);
                }
                if(list.getCategory().equals(mContext.getString(R.string.categoryVegetables))){
                    mealImage.setImageResource(R.mipmap.vegetables);
                }
                if(list.getCategory().equals(mContext.getString(R.string.categoryDessert))){
                    mealImage.setImageResource(R.mipmap.sweets);
                }
                if(list.getCategory().equals(mContext.getString(R.string.categoryDip))){
                    mealImage.setImageResource(R.mipmap.dipping);
                }
                if(list.getCategory().equals(mContext.getString(R.string.categorySalad))){
                    mealImage.setImageResource(R.mipmap.salat);
                }
                if(list.getCategory().equals(mContext.getString(R.string.categoryPasta))){
                    mealImage.setImageResource(R.mipmap.spaghetti);
                }
            }
            if(list.getComment().equals("") || list.getComment() == null){
                commentImage.setImageResource(0);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_card_layout, viewGroup, false);
        return new CardViewHolderHistory(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int i) {
        ((CardViewHolderHistory)viewHolder).setHistoryData(historyLists.get(i), mContext);
        // Set a click listener for item remove button
        ((CardViewHolderHistory)viewHolder).deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
            new AlertDialog.Builder(view.getContext())
                    .setTitle((R.string.myHistory_delete_AlertDialog_title))
                    .setMessage(R.string.myHistory_delete_AlertDialog_message)
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int p) {
                            // Remove the item on remove/button click
                            historyLists.remove(i);
                            notifyItemRemoved(i);
                            notifyItemRangeChanged(i,historyLists.size());
                            // save new ArrayList to Shared Preferences
                            SharedPreferences prefs = view.getContext().getSharedPreferences(PREFS_NAME_HISTORY, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();
                            Gson gson = new Gson();
                            String json = gson.toJson(historyLists);
                            editor.putString(FAVORITE_HISTORY, json);
                            editor.apply();
                        }
                    }).create().show();
            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String stringDate = dateFormat.format(historyLists.get(i).getDate());
                Intent intent = new Intent(view.getContext(), MyHistoryDetail.class);
                intent.putExtra("historyMealName", historyLists.get(i).getMealName());
                intent.putExtra("historyMealPersons", historyLists.get(i).getPersons());
                intent.putExtra("historyMealCategory", historyLists.get(i).getCategory());
                intent.putExtra("historyMealComment", historyLists.get(i).getComment());
                intent.putExtra("historyMealDate", stringDate);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return historyLists.size();
    }

}
