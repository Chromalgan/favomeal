package com.example.fabian.myapplication.appData;

import java.io.Serializable;

/**
 * Class Ingredient, which represents a POJO class for ingredients
 */
public class Ingredient implements Serializable {

    String ingredientName;
    String amount;
    String unit;

    /**
     *  constructor
     *
     * @param ingredientName name of ingredient
     * @param amount amount of ingredient
     * @param unit unit of ingredient
     */
    public Ingredient(String ingredientName, String amount, String unit) {
        this.ingredientName = ingredientName;
        this.amount = amount;
        this.unit = unit;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
