package com.example.fabian.myapplication.appData;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fabian.myapplication.R;

import java.util.ArrayList;

/**
 * adapter class IngredientAdapter for recyclerView to fill it with content
 */
public class IngredientAdapter extends RecyclerView.Adapter {

    ArrayList<Ingredient> ingredients;
    private Context mContext;
    Boolean deleteIconFlag;
    Boolean enableSwitch;
    Button btn_finish;
    private int numberOfIngredients;
    private int numberOfIngredientsSetTrue;
    // sparse boolean array for checking the state of the items
    private static SparseBooleanArray itemSwitchStateArray;

    /**
     * constructor
     *
     * @param context context
     * @param ingredients list of ingredients
     * @param deleteIconFlag boolean to display delete icon or not
     * @param enableSwitchFlag boolean to display switch or not
     * @param btn_finish button to finish list
     */
    public IngredientAdapter(Context context, ArrayList<Ingredient> ingredients, Boolean deleteIconFlag, Boolean enableSwitchFlag, Button btn_finish) {
        this.ingredients = ingredients;
        this.mContext = context;
        this.deleteIconFlag = deleteIconFlag;
        this.enableSwitch = enableSwitchFlag;
        this.numberOfIngredients = ingredients.size();
        this.btn_finish = btn_finish;
        itemSwitchStateArray = new SparseBooleanArray();
    }

    /**
     * class CardViewHolderIngredients to to fill card elements with data
     */
    public static class CardViewHolderIngredients extends RecyclerView.ViewHolder{
        ImageView deleteIcon;
        TextView ingrName;
        TextView ingrAmount;
        TextView ingrUnit;
        Switch ingredientSwitch;
        Button finishShopping;

        private CardViewHolderIngredients(View view, Boolean enableSwitch, Boolean deleteIconFlag, Button finishShopping) {
            super(view);
            if (enableSwitch) {
                ingredientSwitch = view.findViewById(R.id.switch_shopListcard);
                this.finishShopping = finishShopping;
            }
            if (deleteIconFlag) {
                deleteIcon = view.findViewById(R.id.ivDeleteIngredient);
            }
            ingrName = view.findViewById(R.id.tvName);
            ingrAmount = view.findViewById(R.id.tvAmount);
            ingrUnit = view.findViewById(R.id.tvUnit);
        }

        /**
         *  set ingredient data in card
         * @param ingredient ingredient object
         */
        private void setIngredientData(Ingredient ingredient) {
            ingrName.setText(ingredient.getIngredientName());
            ingrAmount.setText(ingredient.getAmount());
            ingrUnit.setText(ingredient.getUnit());
        }

        /**
         * if all switches are checked true then button is activated
         * @param allChecked boolean
         */
        private void changeFinishButton(Boolean allChecked){
            if(allChecked){
                finishShopping.setBackgroundResource(R.drawable.button_rounded_corners);
                finishShopping.setEnabled(true);
            } else {
                finishShopping.setBackgroundResource(R.drawable.button_rounded_corners_disabled);
                finishShopping.setEnabled(false);
            }
        }

        /**
         * set switch state if recyclerView elements were build
         * @param position index of element position
         */
        private void bind(int position){
            if (!itemSwitchStateArray.get(position, false)) {
                ingredientSwitch.setChecked(false);}
            else {
                ingredientSwitch.setChecked(true);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if(enableSwitch){
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.shopping_list_card_layout, viewGroup, false);
        } else if (deleteIconFlag){
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ingredient_card_delete_layout, viewGroup, false);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ingredient_card_layout, viewGroup, false);
        }
        CardViewHolderIngredients vh = new CardViewHolderIngredients(view, enableSwitch, deleteIconFlag, btn_finish);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int i) {
        ((CardViewHolderIngredients)viewHolder).setIngredientData(ingredients.get(i));
        // Set a click listener for item remove button
        if(deleteIconFlag){
            ((CardViewHolderIngredients)viewHolder).deleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Ingredient ingredientItem = ingredients.get(i);
                    ingredients.remove(i);
                    notifyItemRemoved(i);
                    notifyItemRangeChanged(i,ingredients.size());
                    Toast.makeText(mContext,ingredientItem.getIngredientName() + " entfernt",Toast.LENGTH_SHORT).show();
                }
            });
        }
        if(enableSwitch){
            ((CardViewHolderIngredients) viewHolder).bind(i);
            ((CardViewHolderIngredients)viewHolder).ingredientSwitch.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    int adapterPosition = viewHolder.getAdapterPosition();
                    if(!itemSwitchStateArray.get(adapterPosition, false)){
                        ((CardViewHolderIngredients) viewHolder).ingredientSwitch.setChecked(true);
                        itemSwitchStateArray.put(adapterPosition, true);
                    } else {
                        ((CardViewHolderIngredients) viewHolder).ingredientSwitch.setChecked(false);
                        itemSwitchStateArray.put(adapterPosition, false);
                    }
                    // check if number of checked elements is equal to number of ingredients
                    if(itemSwitchStateArray.size() == numberOfIngredients){
                        // check if all switches were set to true
                        numberOfIngredientsSetTrue = 0;
                        for(int counter = 0; counter < itemSwitchStateArray.size(); counter++){
                            if(itemSwitchStateArray.get(counter)){
                                numberOfIngredientsSetTrue++;
                            }
                        }
                        // all switches are checked
                        if(numberOfIngredients == numberOfIngredientsSetTrue){
                            ((CardViewHolderIngredients) viewHolder).changeFinishButton(true);
                        } else {
                            ((CardViewHolderIngredients) viewHolder).changeFinishButton(false);
                        }
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return ingredients.size();
    }
}
