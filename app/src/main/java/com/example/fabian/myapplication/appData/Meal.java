package com.example.fabian.myapplication.appData;

import java.util.ArrayList;

/**
 * Class Meal, which represents a POJO class for meals
 */
public class Meal implements Cloneable {
    String mealName;
    String mealCategory;
    ArrayList<Ingredient> ingredients;
    String mealPersons;

    /**
     * constructor
     *
     * @param mealName name of meal
     * @param mealCategory  category of meal
     * @param ingredients list of ingredients
     * @param mealPersons amount of persons for meal
     */
    public Meal(String mealName, String mealCategory, ArrayList<Ingredient> ingredients, String mealPersons) {
        this.mealName = mealName;
        this.mealCategory = mealCategory;
        this.ingredients = ingredients;
        this.mealPersons = mealPersons;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public String getMealCategory() {
        return mealCategory;
    }

    public void setMealCategory(String mealCategory) {
        this.mealCategory = mealCategory;
    }

    public String getMealPersons() {
        return mealPersons;
    }
    public void setMealPersons(String mealPersons) {
        this.mealPersons = mealPersons;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
