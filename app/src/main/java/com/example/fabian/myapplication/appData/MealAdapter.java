package com.example.fabian.myapplication.appData;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fabian.myapplication.MyMealsDetail;
import com.example.fabian.myapplication.R;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * adapter class MealAdapter for recyclerView to fill it with content
 */
public class MealAdapter extends RecyclerView.Adapter {

    ArrayList<Meal> meals;
    private Context mContext;
    public static final String PREFS_NAME = "MY_APP_STORAGE";
    public static final String FAVORITE_MEALS = "MyMeals";

    /**
     * constructor
     * @param context context
     * @param meals list of meals
     */
    public MealAdapter(Context context, ArrayList<Meal> meals) {
        this.mContext = context;
        this.meals = meals;
    }

    /**
     * class CardViewHolderMeals to to fill card elements with data
     */
    public static class CardViewHolderMeals extends RecyclerView.ViewHolder{
        public ImageView deleteIcon;
        public ImageView mealImage;
        public TextView mealName;

        public CardViewHolderMeals(View view) {
            super(view);
            deleteIcon = view.findViewById(R.id.btn_deleteInCard);
            mealImage = view.findViewById(R.id.mealImage);
            mealName = view.findViewById(R.id.mealName);
        }

        /**
         * set data in card
         * @param meal meal object
         * @param mContext context
         */
        public void setMealData(Meal meal, Context mContext) {
            mealName.setText(meal.getMealName());
            if(meal.getMealCategory() != null){
                if(meal.getMealCategory().equals(mContext.getString(R.string.categoryStew))){
                    mealImage.setImageResource(R.mipmap.eintopf);
                }
                if(meal.getMealCategory().equals(mContext.getString(R.string.categoryFish))){
                    mealImage.setImageResource(R.mipmap.fish);
                }
                if(meal.getMealCategory().equals(mContext.getString(R.string.categoryMeat))){
                    mealImage.setImageResource(R.mipmap.cow);
                }
                if(meal.getMealCategory().equals(mContext.getString(R.string.categoryVegetables))){
                    mealImage.setImageResource(R.mipmap.vegetables);
                }
                if(meal.getMealCategory().equals(mContext.getString(R.string.categoryDessert))){
                    mealImage.setImageResource(R.mipmap.sweets);
                }
                if(meal.getMealCategory().equals(mContext.getString(R.string.categoryDip))){
                    mealImage.setImageResource(R.mipmap.dipping);
                }
                if(meal.getMealCategory().equals(mContext.getString(R.string.categorySalad))){
                    mealImage.setImageResource(R.mipmap.salat);
                }
                if(meal.getMealCategory().equals(mContext.getString(R.string.categoryPasta))){
                    mealImage.setImageResource(R.mipmap.spaghetti);
                }
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.meal_card_layout, viewGroup, false);
        return new CardViewHolderMeals(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int i) {
        ((CardViewHolderMeals)viewHolder).setMealData(meals.get(i), mContext);
        // Set a click listener for item remove button
        ((CardViewHolderMeals)viewHolder).deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                new AlertDialog.Builder(view.getContext())
                    .setTitle((R.string.myMeals_delete_AlertDialog_title))
                    .setMessage(R.string.myMeals_delete_AlertDialog_message)
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int p) {
                            // Remove the item on remove/button click
                            meals.remove(i);
                            notifyItemRemoved(i);
                            notifyItemRangeChanged(i,meals.size());
                            // save new ArrayList to Shared Preferences
                            SharedPreferences prefs = view.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();
                            Gson gson = new Gson();
                            String json = gson.toJson(meals);
                            editor.putString(FAVORITE_MEALS, json);
                            editor.apply();
                        }
                    }).create().show();
            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MyMealsDetail.class);
                Gson gson = new Gson();
                String meal = gson.toJson(meals.get(i));
                intent.putExtra("selectedMeal", meal);
                intent.putExtra("intentFromMyMeals", "intentFlag");
                intent.putExtra("mealIndex", i);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return meals.size();
    }

}
