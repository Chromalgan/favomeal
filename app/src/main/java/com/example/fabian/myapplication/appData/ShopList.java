package com.example.fabian.myapplication.appData;

/**
 * Class ShopList, which represents a POJO class for shoppinglists
 */
public class ShopList {

    Meal shoppingList;
    String mealPersonsForShoppingList;

    /**
     * constructor
     *
     * @param shoppingList meal for list
     * @param mealPersonsForShoppingList amount of persons for list
     */
    public ShopList(Meal shoppingList, String mealPersonsForShoppingList) {
        this.shoppingList = shoppingList;
        this.mealPersonsForShoppingList = mealPersonsForShoppingList;
    }

    public Meal getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(Meal shoppingList) {
        this.shoppingList = shoppingList;
    }

    public String getMealPersonsForShoppingList() {
        return mealPersonsForShoppingList;
    }

    public void setMealPersonsForShoppingList(String mealPersonsForShoppingList) {
        this.mealPersonsForShoppingList = mealPersonsForShoppingList;
    }


}
