package com.example.fabian.myapplication.appData;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fabian.myapplication.MyListsDetail;
import com.example.fabian.myapplication.R;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * adapter class ShopListAdapter for recyclerView to fill it with content
 */
public class ShopListAdapter extends RecyclerView.Adapter {

    ArrayList<ShopList> shoppingLists;
    private Context mContext;
    public static final String PREFS_NAME_LIST = "MY_APP_STORAGE_LIST";
    public static final String FAVORITE_LISTS = "MyLists";

    /**
     * constructor
     * @param context context
     * @param shoppingLists list of shopList
     */
    public ShopListAdapter(Context context, ArrayList<ShopList> shoppingLists) {
        this.mContext = context;
        this.shoppingLists = shoppingLists;
    }
    /**
     * class CardViewHolderLists to to fill card elements with data
     */
    public static class CardViewHolderLists extends RecyclerView.ViewHolder{
        public ImageView deleteIcon;
        public ImageView mealImage;
        public TextView mealName;

        public CardViewHolderLists(View view) {
            super(view);
            deleteIcon = view.findViewById(R.id.listcrad_deleteIcon);
            mealImage = view.findViewById(R.id.listcard_Image);
            mealName = view.findViewById(R.id.tv_listcard_name);
        }

        /**
         * set data in list card
         * @param list list object
         * @param mContext context
         */
        public void setListData(ShopList list, Context mContext) {
            mealName.setText(list.getShoppingList().getMealName());
            if(list.getShoppingList().getMealCategory() != null){
                if(list.getShoppingList().getMealCategory().equals(mContext.getString(R.string.categoryStew))){
                    mealImage.setImageResource(R.mipmap.eintopf);
                }
                if(list.getShoppingList().getMealCategory().equals(mContext.getString(R.string.categoryFish))){
                    mealImage.setImageResource(R.mipmap.fish);
                }
                if(list.getShoppingList().getMealCategory().equals(mContext.getString(R.string.categoryMeat))){
                    mealImage.setImageResource(R.mipmap.cow);
                }
                if(list.getShoppingList().getMealCategory().equals(mContext.getString(R.string.categoryVegetables))){
                    mealImage.setImageResource(R.mipmap.vegetables);
                }
                if(list.getShoppingList().getMealCategory().equals(mContext.getString(R.string.categoryDessert))){
                    mealImage.setImageResource(R.mipmap.sweets);
                }
                if(list.getShoppingList().getMealCategory().equals(mContext.getString(R.string.categoryDip))){
                    mealImage.setImageResource(R.mipmap.dipping);
                }
                if(list.getShoppingList().getMealCategory().equals(mContext.getString(R.string.categorySalad))){
                    mealImage.setImageResource(R.mipmap.salat);
                }
                if(list.getShoppingList().getMealCategory().equals(mContext.getString(R.string.categoryPasta))){
                    mealImage.setImageResource(R.mipmap.spaghetti);
                }
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_card_layout, viewGroup, false);
        return new CardViewHolderLists(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int i) {
        ((CardViewHolderLists)viewHolder).setListData(shoppingLists.get(i), mContext);
        // Set a click listener for item remove button
        ((CardViewHolderLists)viewHolder).deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                new AlertDialog.Builder(view.getContext())
                        .setTitle((R.string.myLists_delete_AlertDialog_title))
                        .setMessage(R.string.myLists_delete_AlertDialog_message)
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int p) {
                                // Remove the item on remove/button click
                                shoppingLists.remove(i);
                                notifyItemRemoved(i);
                                notifyItemRangeChanged(i,shoppingLists.size());
                                // save new ArrayList to Shared Preferences
                                SharedPreferences prefs = view.getContext().getSharedPreferences(PREFS_NAME_LIST, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = prefs.edit();
                                Gson gson = new Gson();
                                String json = gson.toJson(shoppingLists);
                                editor.putString(FAVORITE_LISTS, json);
                                editor.apply();
                            }
                        }).create().show();
            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MyListsDetail.class);
                Gson gson = new Gson();
                String meal = gson.toJson(shoppingLists.get(i).getShoppingList());
                intent.putExtra("selectedList", meal);
                intent.putExtra("selectedListPersons", shoppingLists.get(i).getMealPersonsForShoppingList());
                intent.putExtra("intentFromMyLists", "intentFlag");
                intent.putExtra("listIndex", i);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return shoppingLists.size();
    }

}
