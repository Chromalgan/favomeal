package com.example.fabian.myapplication.popup_windows;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.fabian.myapplication.R;

/**
 * Class PopUpWindowAddIngredient, which represents the Activity as popup to add ingredients to a new Meal
 */
public class PopUpWindowAddIngredient extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    int width, height;
    Button btn_add_ingredient;
    EditText tf_inputIngredientName, tf_inputIngredientAmount;
    Spinner spinner_unit;
    Intent intent;
    String[] units;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_window_add_ingredient);

        units = getApplicationContext().getResources().getStringArray(R.array.unit_array);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        width = dm.widthPixels;
        height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.9), (int) (height*0.52));

        tf_inputIngredientName = findViewById(R.id.tf_Name_Ingr);
        tf_inputIngredientAmount = findViewById(R.id.tf_Amount_Ingr);
        btn_add_ingredient = findViewById(R.id.btn_add_Ingr);
        spinner_unit = findViewById(R.id.spinner_Unit);

        ArrayAdapter<String> spinnerArrayAdapterUnits = new ArrayAdapter<String>(
                this,R.layout.spinner_item, units);
        spinnerArrayAdapterUnits.setDropDownViewResource(R.layout.spinner_item);
        spinner_unit.setAdapter(spinnerArrayAdapterUnits);

        btn_add_ingredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tf_inputIngredientName.getText().toString().equals("") &&
                    !tf_inputIngredientAmount.getText().toString().equals("") &&
                    !spinner_unit.getSelectedItem().toString().equals(getString(R.string.unit_check))) {
                        if(tf_inputIngredientAmount.getText().toString().matches("[0-9]+")){
                            intent = new Intent();
                            intent.putExtra("ingrName", tf_inputIngredientName.getText().toString());
                            intent.putExtra("ingrAmount", tf_inputIngredientAmount.getText().toString());
                            intent.putExtra("ingrUnit", spinner_unit.getSelectedItem().toString());
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.addMeal_addIngredient_wrongAmount_message), Toast.LENGTH_SHORT).show();
                        }
                }
                else {
                    Toast.makeText(getApplicationContext(), getString(R.string.addMeal_addIngredient_EmptyField), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
