package com.example.fabian.myapplication.popup_windows;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.fabian.myapplication.R;

/**
 * Class PopUpWindowAddName, which represents the Activity as popup to add name, category and persons to a new meal
 */
public class PopUpWindowAddName extends AppCompatActivity {

    int width, height;
    Button btn_add_name;
    EditText tf_inputName;
    Spinner spinnerCategory, spinnerPersons;
    Intent intent;
    String[] categories;
    String[] persons;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_window_add_name);

        categories = getApplicationContext().getResources().getStringArray(R.array.category_array);
        persons = getApplicationContext().getResources().getStringArray(R.array.persons_array);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        width = dm.widthPixels;
        height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.9), (int) (height*0.5));

        tf_inputName = findViewById(R.id.tf_inputName);
        btn_add_name = findViewById(R.id.btn_Popup_addName);
        spinnerCategory = findViewById(R.id.spinnerCategory);
        spinnerPersons = findViewById(R.id.spinner_choose_persons);

        // Initializing the ArrayAdapter
        ArrayAdapter<String> spinnerArrayAdapterCategory = new ArrayAdapter<String>(
                this,R.layout.spinner_item, categories);
        spinnerArrayAdapterCategory.setDropDownViewResource(R.layout.spinner_item);
        spinnerCategory.setAdapter(spinnerArrayAdapterCategory);

        ArrayAdapter<String> spinnerArrayAdapterPersons = new ArrayAdapter<String>(
                this,R.layout.spinner_item, persons);
        spinnerArrayAdapterPersons.setDropDownViewResource(R.layout.spinner_item);
        spinnerPersons.setAdapter(spinnerArrayAdapterPersons);

        btn_add_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tf_inputName.getText().toString().equals("") &&
                        !spinnerCategory.getSelectedItem().toString().equals(getString(R.string.category_check)) &&
                        !spinnerPersons.getSelectedItem().toString().equals(getString(R.string.persons_check))) {
                            intent = new Intent();
                            intent.putExtra("mealName", tf_inputName.getText().toString());
                            intent.putExtra("mealCategory", spinnerCategory.getSelectedItem().toString());
                            intent.putExtra("mealPersons", spinnerPersons.getSelectedItem().toString());
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                }
                else {
                    Toast.makeText(getApplicationContext(), getString(R.string.addMeal_saveName_NoName), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
