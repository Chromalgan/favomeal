package com.example.fabian.myapplication.popup_windows;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.fabian.myapplication.R;

/**
 * Class PopUpWindowFinishShopList, which represents the Activity as popup to finish shopList and add comment
 */
public class PopUpWindowFinishShopList extends AppCompatActivity {

    int width, height;
    EditText et_CommentField;
    Button btn_finish;
    Intent intent;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_window_finish_shop_list);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        width = dm.widthPixels;
        height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.9), (int) (height*0.5));

        et_CommentField = findViewById(R.id.eT_Comment);
        btn_finish = findViewById(R.id.btn_finish_list);

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent();
                intent.putExtra("listComment", et_CommentField.getText().toString());
                intent.putExtra("listIndex", getIntent().getIntExtra("listIndex",999));
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

    }
}
