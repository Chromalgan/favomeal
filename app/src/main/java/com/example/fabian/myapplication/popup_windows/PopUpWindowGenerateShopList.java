package com.example.fabian.myapplication.popup_windows;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.fabian.myapplication.R;

/**
 * Class PopUpWindowGenerateShopList, which represents the Activity as popup to generate a shoplist for a meal
 */
public class PopUpWindowGenerateShopList extends AppCompatActivity {

    int width, height;
    Button btn_generate_List;
    Spinner spinnerPersons;
    Intent intent;
    String[] persons;

    /**
     * onCreate method, which is called when activity is loaded
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_window_generate_shop_list);

        persons = getApplicationContext().getResources().getStringArray(R.array.persons_array);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        width = dm.widthPixels;
        height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.9), (int) (height*0.4));

        btn_generate_List = findViewById(R.id.btn_Popup_generateList);
        spinnerPersons = findViewById(R.id.spinner_persons_list);

        // Initializing an ArrayAdapter
        ArrayAdapter<String> spinnerArrayAdapterPersons = new ArrayAdapter<String>(
                this,R.layout.spinner_item, persons);
        spinnerArrayAdapterPersons.setDropDownViewResource(R.layout.spinner_item);
        spinnerPersons.setAdapter(spinnerArrayAdapterPersons);

        btn_generate_List.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!spinnerPersons.getSelectedItem().toString().equals(getString(R.string.persons_check))) {
                    intent = new Intent();
                    intent.putExtra("personsForList", spinnerPersons.getSelectedItem().toString());
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
                else {
                    Toast.makeText(getApplicationContext(), getString(R.string.myMealDetail_generate_shopping_list_no_persons), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
